﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShapeSpawner : MonoBehaviour
{
    public GameObject[] shapes;

    public GameObject[] nextShapes;

    GameObject upNext = null;
    GameObject upNext_1 = null;

    int shapeIndex = 0;
    int nextShapeIndex = 0;
    int nextShapeIndex_1 = 0;

    public void SpawnShapes()
    {
        int nextShapeIndex = nextShapeIndex_1;
        int shapeIndex = nextShapeIndex;
        

        Instantiate(shapes[shapeIndex],
            transform.position,
            Quaternion.identity);
        nextShapeIndex_1 = Random.Range(0, 7);
        Vector3 nextShapePos = new Vector3(-9.35f, 14, 0);
        Vector3 nextShapePos_1 = new Vector3(-9.35f, 7, 0);
        if (upNext != null)
            Destroy(upNext);
        if (upNext_1 != null)
            Destroy(upNext_1);
        upNext = Instantiate(nextShapes[nextShapeIndex], nextShapePos, Quaternion.identity);
        upNext_1 = Instantiate(nextShapes[nextShapeIndex_1], nextShapePos_1, Quaternion.identity);
    }

    // Start is called before the first frame update
    void Start()
    {
        nextShapeIndex_1 = Random.Range(0, 7);
        SpawnShapes();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
